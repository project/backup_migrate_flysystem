# Backup and Migrate: Flysystem

This module allows for any Flysystem filesystem to be used as a destination for Backup and Migrate files.

## Requirements ##

- [Backup and Migrate](https://www.drupal.org/project/backup_migrate)
- [Flysystem](https://www.drupal.org/project/flysystem)
- A supported adapter module. Examples:
    - [Flysystem - S3](https://www.drupal.org/project/flysystem_s3)
    - [Flysystem - Dropbox](https://www.drupal.org/project/flysystem_dropbox)
    - [Flysystem - SFTP](https://www.drupal.org/project/flysystem_sftp)

## Installation

It is suggested that you install using Composer.

```bash
cd /path/to/drupal/root
composer require drupal/backup_migrate_flysystem
drush en backup_migrate_flysystem
```

## Configuration ##

1. Configure one or more Flysystem filesystems (also called stream wrappers, or schemes).

    Flysystem filesystems are configured in settings.php. The Flysystem module's  [README](http://cgit.drupalcode.org/flysystem/plain/README.md?h=8.x-1.x) is a good source of configuration examples.

    To use an AWS S3 bucket, your Flysystem filesystem should look like:
    ```php
    $settings['flysystem']['s3-example'] = [
        'name' => 'S3 Example',
        'driver' => 's3',
        'config' => [
          'key'    => 'your_aws_key',
          'secret' => 'your_aws_key_secret',
          'region' => 'us-east-1',
          'bucket' => 'bucket-name',
          'protocol' => "https",
          'cname_is_bucket' => FALSE,
          'use_path_style_endpoint' => FALSE,
          'public' => FALSE,
          'prefix' => 'path/to/backups',
        ],
        'cache' => TRUE,
        'serve_js' => FALSE,
        'serve_css' => FALSE,
    ];
    ```
2. Visit the Backup and Migrate Destinations settings page (/admin/config/development/backup_migrate/settings/destination)

3. Add a new Backup Destination and choose "Flysystem" as the type.

4. Select your previously configured Flysystem filesystem from the select menu. In the above example, it will show as 'S3 Example'.

5. You can optionally include a Directory Prefix Path to the Backup Destination's configuration. This directory will be added to filesystem prefix that you may have specified in settings.php.

### Example configuration for Backblaze B2 Cloud Storage

The [Backblaze](https://www.backblaze.com) company provides a file storage
system called "B2 Cloud Storage" which is compatible with the S3 API v4.

In order for this to work the [Flysystem
S3](https://www.drupal.org/project/flysystem_s3) module must also be installed.

The following Flysystem configuration has worked in our testing:
```php
    // This array key can be changed as needed, it is how the module will
    // identify this backup destination.
    $settings['flysystem']['backblaze'] = [
      // This must be set to "s3" and the Flysystem S3 module must also be
      // installed.
      'driver' => 's3',
      'config' => [
        // Set this to the "keyID" value from Backblaze.
        'key' => 'MYKEYID',
        // Set this to the "applicationKey" value from Backblaze.
        'secret' => 'MYAPPLICATIONKEY',
        // The name of the bucket created in Backblaze.
        'bucket' => 'MYBUCKETNAME',
        // This should be taken from the hostname provided by Backblaze; it
        // might not need to be changed.
        'region' => 'us-west',
        // The hostname provided by Backblaze - make sure to prefix it with
        // "https://" otherwise the backups won't work.
        'endpoint' => 'https://s3.us-west-000.backblazeb2.com',
      ],
      // Creates a metadata cache to speed up lookups.
      'cache' => TRUE,
    ];
```
