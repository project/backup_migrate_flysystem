<?php

namespace Drupal\backup_migrate_flysystem\Plugin\BackupMigrateDestination;

use Drupal\backup_migrate\Drupal\EntityPlugins\DestinationPluginBase;

/**
 * Defines a file directory destination plugin.
 *
 * @BackupMigrateDestinationPlugin(
 *   id = "flysystem",
 *   title = @Translation("Flysystem"),
 *   description = @Translation("Back up to a directory in a Flysystem storage stream."),
 *   wrapped_class = "\Drupal\backup_migrate_flysystem\Destination\FlysystemDestination"
 * )
 *
 * Note: It is unclear why the plugin is constructed with wrapped_class. To keep
 * things consistent, the deprecated Psr4ClassLoader is called in
 * backup_migrate_flysystem.module.
 *
 * @see \Drupal\backup_migrate\Drupal\EntityPlugins\WrapperPluginBase::getObject()
 *
 */
class FlysystemDestinationPlugin extends DestinationPluginBase {}
