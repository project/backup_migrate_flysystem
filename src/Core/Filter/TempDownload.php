<?php

namespace Drupal\backup_migrate_flysystem\Core\Filter;

use Drupal\backup_migrate\Core\File\BackupFileReadableInterface;
use Drupal\backup_migrate\Core\Plugin\FileProcessorInterface;
use Drupal\backup_migrate\Core\Plugin\FileProcessorTrait;
use Drupal\backup_migrate\Core\Plugin\PluginBase;
use Drupal\backup_migrate_flysystem\File\ReadableFlysystemBackupFile;

/**
 *
 * Not all Flysystem filesystems are going to support seek and
 * the CompressionFilter plugin is likely going to initiate fseek operations
 * during decompression of the file. This is generally just a good idea:
 *    Download the file as a temp file.
 *
 * Cleanup will happen automatically.
 *
 * @package Drupal\backup_migrate_flystem\Core\Filter
 */
class TempDownload extends PluginBase implements FileProcessorInterface {

  use FileProcessorTrait;

  /**
   *
   */
  public function supportedOps() {
    return [
      'beforeRestore' => ['weight' => -10000],
    ];
  }

  /**
   *
   */
  /**
   * @param BackupFileReadableInterface|ReadableFlysystemBackupFile $file
   *
   * @return BackupFileReadableInterface|ReadableFlysystemBackupFile
   * @throws \Exception
   */
  public function beforeRestore(BackupFileReadableInterface $file) {

    // Verify that this is a restore for a Flysystem backup file.
    // If not, no need to do anything with it.
    if (!$file instanceof ReadableFlysystemBackupFile) {
      return $file;
    }

    $file->openForRead();
    $tempFile = $this->getTempFileManager()->popExt($file);
    $tempFile->openForWrite();
    $tempFile->writeAll($file->readAll());
    $file->setTemp($tempFile);

    return $file;
  }

}
