<?php

namespace Drupal\backup_migrate_flysystem\Destination;

use Drupal\backup_migrate\Core\Config\ConfigInterface;
use Drupal\backup_migrate\Core\Destination\DirectoryDestination;
use Drupal\backup_migrate\Core\Exception\BackupMigrateException;
use Drupal\backup_migrate\Core\Exception\DestinationNotWritableException;
use Drupal\backup_migrate\Core\File\BackupFileInterface;
use Drupal\backup_migrate\Core\File\BackupFileReadableInterface;
use Drupal\backup_migrate_flysystem\File\ReadableFlysystemBackupFile;
use Drupal\flysystem\Flysystem\Adapter\MissingAdapter;
use League\Flysystem\Exception;


/**
 * Class FlysystemDestination.
 *
 * @package Drupal\backup_migrate\Drupal\Destination
 */
class FlysystemDestination extends DirectoryDestination {

  /**
   * @var \Drupal\flysystem\FlysystemFactory
   */
  private $flysystemFactory;

  /**
   * @var \League\Flysystem\Filesystem
   */
  private $flysystem;

  /**
   * @var string
   */
  private $scheme;

  /**
   * @var string
   */
  private $path;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigInterface $config) {
    parent::__construct($config);

    $this->flysystemFactory = \Drupal::service('flysystem_factory');

    $this->scheme = $this->config->get('scheme');
    $this->path = $this->config->get('path');
    $this->flysystem = $this->flysystemFactory->getFilesystem($config->get('scheme'));
  }

  /**
   * Do the actual file save. This function is called to save the data file AND
   * the metadata sidecar file.
   *
   * @param \Drupal\backup_migrate\Core\File\BackupFileReadableInterface $file
   *
   * @throws \Drupal\backup_migrate\Core\Exception\BackupMigrateException
   */
  function saveTheFile(BackupFileReadableInterface $file) {
    // Check if the directory exists.
    $this->checkDirectory();

    $path = $this->confGet('path');
    $destination_path =  (!empty($path) ?  $path . '/' : '') . $file->getFullName();

    // Must close file because meta files appear to be left open by
    // backup_migrate and Flysystem will need to rewind.
    $file->close();

    // Put/Upload the file to the Flysystem.
    set_time_limit(0); // Just in case this takes a while.
    $this->flysystem->putStream($destination_path, $file->openForRead());
    $file->close();

  }

  /**
   * {@inheritdoc}
   */
  public function deleteTheFile($id): bool {
    if ($file = $this->getFile($id)) {
      if ($this->flysystem->has($this->idToPath($id))) {
        return $this->flysystem->delete($this->idToPath($id));
      }
    }
    return FALSE;
  }

  /**
   * Get the entire file list from this destination.
   *
   * @return array
   */
  protected function getAllFileNames() {

    // Read the list of files from the filesystem.
    $files = [];
    $contents = $this->checkDirectory();
    foreach($contents as $file) {
      if (substr($file['basename'], 0, 1) !== '.' && substr($file['basename'], strlen($file['basename']) - 5) !== '.info') {
        $files[] = $file['basename'];
      }
    }

    return $files;
  }

  /**
   * {@inheritdoc}
   */
  public function fileExists($id) {
    return $this->flysystem->has($this->idToPath($id));
  }

  /**
   * {@inheritdoc}
   */
  protected function idToPath($id) {
    $file_path = ltrim($this->path  . '/' . $id, '/');
    return rtrim($file_path, '/') ;
  }

  /**
   * Check that the directory can be used for backup.
   *
   * Also returns the Flysystem directory contents.
   *
   * @return array|void
   * @throws \Drupal\backup_migrate\Core\Exception\BackupMigrateException
   */
  protected function checkDirectory() {

    // Check if Flysystem is configured properly.
    if ($this->flysystem->getAdapter() instanceof MissingAdapter) {
      throw new BackupMigrateException(
        "The backup file could not be saved to '%uri' because the Flysystem filesystem is not valid or is not configured correctly.",
        ['%uri' => $this->confGet('scheme') . '://' . $this->confGet('path')]
      );
    }

    // Check connection. The directory might not exist, but this should at
    // least return an empty list if the connection is working correctly.
    // This might be a bit slow to do, but Flysystem doesn't appear to have
    // another way of checking the connection. For most Flysystems, this
    // should return quickly.
    try {
      $contents = $this->flysystem->listContents($this->confGet('path'), FALSE);
    } catch (\Exception $exception) {
      throw new BackupMigrateException(
        "Connection check failed. Please check your configuration of '%uri'. \n" .
        $exception->getMessage(),
        ['%uri' => $this->confGet('scheme') . '://' . $this->confGet('path')]
      );
    }
    return $contents;
  }

  /**
   * Check if the Flysystem filesystem is actually writable.
   */
  public function checkWritable() {
    $this->checkDirectory();

    $path = $this->confGet('path');
    $destination_path =  (!empty($path) ?  $path . '/' : '') . 'test_if_writable';

    try {
      $this->flysystem->put($destination_path, 'Testing if Flysystem is writable', []);
      $this->flysystem->delete($destination_path);
    }
    catch (\Exception $exception) {
      throw new DestinationNotWritableException(
        "The backup file could not be saved to '%uri' because Backup and Migrate does not have write access to that directory or there is something wrong with the Flysystem configuration.",
        ['%uri' => $this->confGet('scheme') . '://' . $this->confGet('path')]
      );
    }

  }

  /**
   * Get a definition for user-configurable settings.
   *
   * @param array $params
   *
   * @return array
   */
  public function configSchema($params = []) {
    $schema = [];

    // Init settings.
    if ($params['operation'] == 'initialize') {
      $schema['fields']['scheme'] = [
        'type' => 'enum',
        'title' => $this->t('Flysystem Filesystem'),
        'options' => $this->getFlysystemsForSelect(),
        'required' => true,
        'description' => $this->t('Flysystem filesystems, sometimes called schemes, must be configured in your settings.php file (or an included file).'),
      ];
      if (empty($schema['fields']['scheme']['options'])) {
        $schema['fields']['scheme']['description'] = $this->t('You must configure Flysystem filesystems first.');
      }
      $schema['fields']['path'] = [
        'type' => 'text',
        'title' => $this->t('Prefix Directory Path'),
        'description' => $this->t('Optional. This will be added to any prefix that is set in the Flysystem filesystem\'s configuration.'),
      ];
    }

    return $schema;
  }

  /**
   * {@inheritdoc}
   * @throws \Exception
   */
  public function loadFileForReading(BackupFileInterface $file) {
    // If this file is already readable, simply return it.
    if ($file instanceof BackupFileReadableInterface) {
      return $file;
    }

    $id = $file->getMeta('id');
    if ($this->fileExists($id)) {
      return new ReadableFlysystemBackupFile($this->idToPath($id), $this);
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function listFiles() {
    $path = $this->confGet('path');
    $out = [];

    // Get the entire list of filenames.
    $files = $this->getAllFileNames();

    foreach ($files as $file) {
      $filepath = $path . '/' . $file;
      $out[$file] = new ReadableFlysystemBackupFile($filepath, $this);
    }

    return $out;
  }

  /**
   * {@inheritdoc}
   */
  public function queryFiles(
    $filters = [],
    $sort = 'datestamp',
    $sort_direction = SORT_DESC,
    $count = 100,
    $start = 0
  ) {

    // This might not work, but it is worth a try in case the remote
    // response is slow.
    set_time_limit(600);

    // Get the full list of files.
    $out = $this->listFiles($count + $start);
    foreach ($out as $key => $file) {
      $out[$key] = $this->loadFileMetadata($file);
    }

    // Filter the output.
    if ($filters) {
      $out = array_filter($out, function($file) use ($filters) {
        foreach ($filters as $key => $value) {
          if ($file->getMeta($key) !== $value) {
            return FALSE;
          }
        }
        return TRUE;
      });
    }

    // Sort the files.
    if ($sort && $sort_direction) {
      uasort($out, function ($a, $b) use ($sort, $sort_direction) {
        if ($sort_direction == SORT_DESC) {
          if ($sort == 'name') {
            return $a->getFullName() < $b->getFullName();
          }
          // @TODO: fix this in core
          return $a->getMeta($sort) < $b->getMeta($sort);
        }
        else {
          if ($sort == 'name') {
            return $b->getFullName() <=> $a->getFullName();
          }
          // @TODO: fix this in core
          return $b->getMeta($sort) <=> $a->getMeta($sort);
        }
      });
    }

    // Slice the return array.
    if ($count || $start) {
      $out = array_slice($out, $start, $count);
    }

    return $out;
  }


  public function getFlysystem() {
    return $this->flysystem;
  }


  private function getFlysystemsForSelect() {

    $schemes = $this->flysystemFactory->getSchemes();
    $flysystems = [];
    foreach($schemes as $scheme) {
      $settings = $this->flysystemFactory->getSettings($scheme);
      $flysystems[$scheme] = !empty($settings['name']) ? $settings['name'] : $scheme;
    }
    return $flysystems;
  }
}
