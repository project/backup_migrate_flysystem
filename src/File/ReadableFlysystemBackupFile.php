<?php
/**
 * @file
 * Contains Drupal\backup_migrate\Core\File\ReadableStream.
 */

namespace Drupal\backup_migrate_flysystem\File;

use Drupal\backup_migrate\Core\File\BackupFile;
use Drupal\backup_migrate\Core\File\BackupFileReadableInterface;
use Drupal\backup_migrate\Core\File\BackupFileWritableInterface;
use Drupal\backup_migrate_flysystem\Destination\FlysystemDestination;

/**
 * Class ReadableFlysystemBackupFile.
 *
 * @package Drupal\backup_migrate\Core\File
 *
 * An implementation of the BackupFileReadableInterface which uses a the
 * Flysystem API to access files. The ReadableStreamBackupFile, which uses
 * PHP file functions, didn't seem to work in all server environments.
 *
 * Modified version of:
 * @see \Drupal\backup_migrate\Core\File\ReadableStreamBackupFile
 */
class ReadableFlysystemBackupFile extends BackupFile implements BackupFileReadableInterface {

  /**
   * A file handle if it is open.
   *
   * @var resource
   */
  protected $handle;

  /**
   * @var FlysystemDestination
   */
  private $destinationPlugin;

  /**
   * @var \League\Flysystem\Filesystem
   */
  private $flysystem;

  /** @var \Drupal\backup_migrate\Core\File\BackupFileWritableInterface */
  private $tempFile;

  /**
   * ReadableFlysystemBackupFile constructor.
   *
   * @param string $filepath string The path to a file (which must already exist). Can be a stream URI.
   * @param FlysystemDestination $destinationPlugin The Flysystem filesystem where this file resides.
   *
   * @throws \Exception
   */
  function __construct($filepath, $destinationPlugin) {

    $this->destinationPlugin = $destinationPlugin;
    $this->flysystem = $this->destinationPlugin->getFlysystem();
    $this->path = $filepath;

    // Check that the file exists and is readable.
    if (!$this->flysystem->has($filepath)) {
      throw new \Exception("The file '$filepath' does not exist or cannot be read.");
    }

    // Get the basename and extensions.
    $this->setFullName(basename($filepath));

    // Get the basic file stats since this is probably a read-only file option and these won't change.
    $this->_loadFileStats();
  }

  /**
   * Destructor.
   */
  function __destruct() {
    // Close the handle if we've opened it.
    $this->close();
  }

  /**
   * Get the realpath of the file. This includes the flysystem scheme.
   *
   * @return string|null The stream URI to the file or NULL if the file does not exist.
   */
  public function realpath() {

    // Restore operations might be trying to use fseek, so return the path
    // to the temp file if it already exists.,
    if ($this->getTemp()) {
      return $this->getTemp()->realpath();
    }
    // Return the path, with Flysystem scheme.
    if ($this->flysystem->has($this->path)) {
      return $this->destinationPlugin->confGet('scheme') . '://' . $this->path;
    }
    return NULL;
  }

  /**
   * Get the localpath of the file. This includes the flysystem scheme.
   *
   * @return string|null The path file or NULL if the file does not exist.
   */
  private function localPath() {
    if ($this->flysystem->has($this->path)) {
      return $this->path;
    }
    return NULL;
  }

  /**
   * Open a file for reading or writing.
   *
   * @param bool $binary If true open as a binary file
   *
   * @return resource
   *
   * @throws \Exception
   */
  function openForRead($binary = FALSE) {
    if (!$this->isOpen()) {
      $path = $this->localPath();

      if (!$path) {
        // @TODO: Throw better exception
        throw new \Exception("The file '$path' does not exist or cannot be read (ReadableFlysystemBackupFile::openForRead).");
      }

      // Open the file.
      $this->handle = $this->flysystem->readStream($path);
      if (!$this->handle) {
        throw new \Exception("Failed to open '$path' (ReadableFlysystemBackupFile::openForRead).");
      }
    }
    // If the file is already open, rewind it.
    $this->rewind();
    return $this->handle;
  }

  /**
   * Close a file when we're done reading/writing.
   */
  function close() {
    if ($this->isOpen()) {
      fclose($this->handle);
      $this->handle = NULL;
    }
  }

  /**
   * Is this file open for reading/writing.
   *
   * Return bool True if the file is open, false if not.
   */
  function isOpen() {
    return !empty($this->handle) && get_resource_type($this->handle) == 'stream';
  }

  /**
   * Read a line from the file.
   *
   * @param int $size The number of bites to read or 0 to read the whole file
   * @param bool $binary
   *
   * @return false|string|null The data read from the file or NULL if the file can't be read or is at the end of the file.
   * @throws \Exception
   */
  function readBytes($size = 1024, $binary = FALSE) {
    if (!$this->isOpen()) {
      $this->openForRead($binary);
    }
    if ($this->handle && !feof($this->handle)) {
      return fread($this->handle, $size);
    }
    return NULL;
  }


  /**
   * Read a single line from the file.
   *
   * @return false|string The data read from the file or FALSE if the file can't be read or is at the end of the file.
   * @throws \Exception
   */
  public function readLine() {
    if (!$this->isOpen()) {
      $this->openForRead();
    }
    return fgets($this->handle);
  }

  /**
   * Read the full file.
   *
   * @return false|string The data read from the file or FALSE if the file can't be read.
   * @throws \Exception
   */
  public function readAll() {
    if (!$this->isOpen()) {
      $this->openForRead();
    }
    $this->rewind();
    return stream_get_contents($this->handle);
  }

  /**
   * Move the file pointer forward a given number of bytes.
   *
   * @param int $bytes
   *
   * @return int
   *  The number of bytes moved or -1 if the operation failed.
   */
  public function seekBytes($bytes) {
    if ($this->isOpen()) {
      return fseek($this->handle, $bytes);
    }
    return -1;
  }

  /**
   * Rewind the file handle to the start of the file.
   *
   * Many Flysystems will not support rewind (S3). Only attempt
   * it if ftell reports back that the file needs to be rewound.
   *
   */
  function rewind() {
    if ($this->isOpen() && ftell($this->handle) !== 0) {
      @rewind($this->handle);
    }
  }

  /**
   * Get info about the file and load them as metadata.
   */
  protected function _loadFileStats() {
    $this->setMeta('filesize', $this->flysystem->getSize($this->localPath()));
    $this->setMeta('datestamp', $this->flysystem->getTimestamp($this->localPath()));
  }


  /**
   * @param \Drupal\backup_migrate\Core\File\BackupFileWritableInterface $tempFile
   *
   * @return void
   */
  public function setTemp(BackupFileWritableInterface $tempFile) {
    $this->tempFile = $tempFile;
  }

  /**
   * @return \Drupal\backup_migrate\Core\File\BackupFileWritableInterface|false
   */
  public function getTemp() {
    return $this->tempFile ?? false;
  }
}
